# ci-mr-pipeline-webhook

A web service to receive Gitlab [Merge Request webhooks][mr-webhooks] and
translate them into [CI pipeline triggers][pipeline-triggers], motivated by
[gitlab-ce#23902][ce#23902].

[pipeline-triggers]: https://docs.gitlab.com/ce/ci/triggers/
[mr-webhooks]: https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#merge-request-events
[ce#23902]: https://gitlab.com/gitlab-org/gitlab-ce/issues/23902